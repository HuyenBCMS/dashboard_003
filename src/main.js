import Vue from 'vue';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;

// Global 3rd party plugins
import 'bootstrap';
import 'popper.js';
import 'tooltip.js';
import 'perfect-scrollbar';

// Vue 3rd party plugins
import './common/plugins/bootstrap-vue';
import './common/plugins/perfect-scrollbar';
import './common/plugins/highlight-js';
import '@babel/polyfill';
import * as am4core from '@amcharts/amcharts4/core';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

/* Chart code */
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('token');
    
    if (to.name === 'login' && !token) {
        return next();
    }

    if (to.name != 'login' && !token) {
        next({ name: 'login' });
    } else if (to.name === 'login' && token) {
        next({ name: 'dashboard' });
    } else if (to.name === 'dashboard' && token) {
        next();     
    }
});

if ('serviceWorker' in navigator) {              
    window.addEventListener('load', () => {
        navigator.serviceWorker
            .register('/service-worker.js')
            .then((registration) => {})
            .catch(() => {});
    });
}

new Vue({
    router,
    render: (h) => h(App),
}).$mount('#app');
