import * as moment from 'moment';
import Swal from "sweetalert2";
import KTPortlet from "@/components/Portlet.vue";
import SearchInput from "@/components/SearchInput.vue";
import axios from "@/common/api.js";
import Loader from "@/common/loader";

export default {
  name: "DailyReport",
  components: {
    KTPortlet,
    SearchInput,
  },
  data() {
    return {
      showCreateLicenseKey: false,
      showCreateSlideshow: false,
      states: [
        {
          value: true,
          text: "Kích hoạt"
        },
        {
          value: false,
          text: "Chưa kích hoạt"
        }
      ],
      labels: {
        vn: {
          labelPrevYear: 'Năm trước',
          labelPrevMonth: 'Tháng trước',
          labelCurrentMonth: 'Tháng hiện tại',
          labelNextMonth: 'Tháng tiếp theo',
          labelNextYear: 'Năm tiếp theo',
          labelToday: 'Hôm nay',
          labelNoDateSelected: 'Không có ngày được chọn',
          labelHelp: 'Sử dụng các phím mũi tên để điều hướng qua lịch.'
        },
      },
      url: null,
      dataLicenseKey: [],
      dataSlidesShow: [],
      fieldsLicenseKey: [
        {
          key: '$index',
          label: 'ID',
        },
        {
          key: 'key.local',
          label: 'License Key',
          class: "license-key",
        },
        {
          key: 'active',
          label: 'Trạng thái',
          class: "status",
        },
        {
          key: 'activeDate',
          label: 'Ngày bắt đầu',
        },
        {
          key: '_id',
          label: '',
        },
      ],
      fieldsSlides: [
        {
          key: '$index',
          label: 'ID',
        },
        {
          key: 'image',
          label: 'Hình ảnh',
        },
        {
          key: 'name',
          label: 'Tiêu đề',
          class: "title-slide",
        },
        {
          key: 'link',
          label: 'Link',
        },
        {
          key: '_id',
          label: '',
        },
      ],
      formClienseKey: {
        deviceId : '',
        name: '',
        phone: '',
        address: '',
        expireDate: '',
        activeDate: '',
        active: false,
        key: '',
      },
      formSlideShow: {
        link : '',
        image: '',
        name: '',
      },
      showEditLicenseKey: false,
      showEditSlidesShow: false,
      fileImage: null,
      dataCurrent: {},
      dataCurrentLicense: {},
      titleLicense: '',
      titleSlide: '',
      loadingSlide: false,
      loadingLicense: false,
    };
  },

  created() {
    this.getListLicenseKey();
    this.getListSlideShow();
  },

  methods: {
    getListLicenseKey(data) {
      this.loadingLicense = !this.loadingLicense;
      const params = data ? data : {};
      axios({
        method: `get`,
        url: 'license',
        params: params
      }).then((res) => {       
        this.dataLicenseKey = res;
        this.loadingLicense = false
      }).finally()
    },

    getListSlideShow(data) {
      this.loadingSlide = !this.loadingSlide;
      const params = data ? data : {};
      axios({
        method: 'get',
        url: 'image',
        params: params,
      }).then((res) => {
        this.dataSlidesShow = res;
        this.loadingSlide = false
      }).finally()
    },

    onSearchLicenseKey(event) {
      const keyword = {
        keyword: event,
      };
      this.getListLicenseKey(keyword);
    },

    onSearchSlideshow(event) {
      const keyword = {
        keyword: event,
      };
      this.getListSlideShow(keyword);
    },

    createLicenseKey() {
      Loader.fire();
      this.showCreateLicenseKey = true;
      this.titleLicense = "Tạo mới License Key"
      this.formClienseKey = {
        name: '',
        phone: '',
        address: '',
        expireDate: '',
        activeDate: '',
        active: false,
        key: '',
      }
      
      axios({
        method: 'get',
        url: 'license/key',
      }).then((res) => {
        this.formClienseKey.key = res.key;
      }).finally(Loader.close)
    },

    handerCreateLicenseKey() {
      Loader.fire();
      let method = 'post';

      if (this.dataCurrentLicense._id) {
        method = 'put',
        this.formClienseKey.id = this.dataCurrentLicense._id
      }

      axios({
        method: method,
        url: 'license',
        data: this.formClienseKey,
      }).then((res) => {
        this.showCreateLicenseKey = false;
        this.getListLicenseKey();
        this.formClienseKey = {
          deviceId : '',
          name: '',
          phone: '',
          address: '',
          expireDate: '',
          activeDate: '',
          active: false,
        }
      }).finally(Loader.close)
    },

    handerCreateSlideshow() {
      Loader.fire();
      let methods = 'post';
      const formData = new FormData();
      this.formSlideShow.image = this.url;
      formData.append('link', this.formSlideShow.link);
      formData.append('image', this.fileImage);
      formData.append('name', this.formSlideShow.name);
      if (this.dataCurrent._id) {
        formData.append('id', this.dataCurrent._id);
        methods = 'put'
      }

      axios({
        method: methods,
        url: 'image',
        data: formData,
      }).then((res) => {
        this.showCreateSlideshow = false;
        this.getListSlideShow();
      }).finally(Loader.close)
    },

    updateLicenseKey(data) {
      this.showCreateLicenseKey = true;
      this.dataCurrentLicense = data;
      this.titleLicense = "Cập nhật License Key"
      
      this.formClienseKey = {
        key : data.key.local,
        name: data.owner.name,
        phone: data.owner.phone,
        address: data.owner.address,
        expireDate: data.expireDate,
        activeDate: data.activeDate,
        active: data.active,
      }
    },

    createSlideshow() {
      this.showCreateSlideshow = true;
      this.url = null;
      this.fileImage = null;
      this.formSlideShow.name = '';
      this.formSlideShow.link = '';
      this.titleSlide = "Tạo mới Slide";
    },

    updateSlideshow(data) {
      this.dataCurrent = data;
      this.titleSlide = " Cập nhật Slide"
      
      this.showCreateSlideshow = true;
      this.formSlideShow.name = data.name;
      this.formSlideShow.link = data.link;
      this.url = data.image;
    },

    checkFormValidity() {
      const valid = this.$refs.form.checkValidity()
      this.nameState = valid
      return valid
    },

    handleOk(bvModalEvt) {
      bvModalEvt.preventDefault()
      this.submitCreateLienseKey()
    },

    submitCreateLienseKey() {
      if (!this.checkFormValidity()) {
        return
      }
      this.$nextTick(() => {
        this.$bvModal.hide('modal-prevent-closing')
      })
    },

    onFileChange(e) {
      this.fileImage = e.target.files[0];
      this.url = URL.createObjectURL(this.fileImage);
    },

    removeImage() {
      this.url = null;
      this.fileImage = null;
    },

    removeImageSlide(data) {
      Swal.fire({
        position: "center",
        icon: "warning",
        title: 'Bạn muốn xóa hình ảnh?',
        allowEscapeKey: false,
        allowOutsideClick: false,
        allowEnterKey: false,
        showCancelButton: true,
        confirmButtonText: 'Đồng ý',
        cancelButtonText: 'Hủy',
      }).then((result) => {
        if (result.value) {
          Loader.fire();
          axios({
            method: 'delete',
            url: 'image',
            data: {id: data._id},
          }).then((res) => {
            this.getListSlideShow();
          }).finally(Loader.close)
        }
      });
    },

    removeLicense(data) {
      Swal.fire({ 
        position: "center",
        icon: "warning",
        title: 'Bạn muốn xóa License Key?',
        allowEscapeKey: false,
        allowOutsideClick: false,
        allowEnterKey: false,                                                                                                                                                                               
        showCancelButton: true,
        confirmButtonText: 'Đồng ý',
        cancelButtonText: 'Hủy',
      }).then((result) => {
        if (result.value) {
          axios({
            method: 'delete',
            url: 'license',
            data: {id: data._id},
          }).then((res) => {
            this.showCreateLicenseKey = false;
            this.getListLicenseKey();
          })
        }
      });
    },

    formatDate(date) {
      if (date) {
        return moment(date).format("DD/MM/YYYY");
      } else {
        return '';
      }
    },
      
  },
  mounted() {
      
  },
  computed: {
  },
};
