import Vue from 'vue';
import Router from 'vue-router';
import { UserRole } from './constans';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            name: 'login',
            path: '/login',
            component: () => import('@/views/auth/Login'),
        },
        {
            path: '/',
            redirect: '/dashboard',
        },
        {
            name: 'dashboard',
            path: '/dashboard',
            component: () => import('@/views/dashboard/DashBoard'),
        },
        {
            path: '*',
            redirect: '/404',
        },
        {
            path: '/404',
            name: 'not-found',
            component: () => import('@/views/error/NotFound.vue'),
        },
    ],
});
