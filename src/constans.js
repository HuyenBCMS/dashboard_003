export const UserRole = {
    Staff:  1,
    Cashier: 2,
    OutletManager: 3,
    OutletBoss: 4,
    CEO: 5
}

export const Status = {
    Removed: 0,
    Actived : 1,
    NotActived: 2,
}

export const StatusCLient = {
    Paid: 2,
    Doing : 1,
    ReadyToPay: 3,
}
