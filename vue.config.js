const workboxPlugin = require("workbox-webpack-plugin");

module.exports = {
  css: {
    loaderOptions: {
      postcss: {
        config: {
          path: __dirname
        }
      }
    }
  },
  lintOnSave: false,
  configureWebpack: {
    plugins: [
      new workboxPlugin.GenerateSW({
        swDest: "service-worker.js",
        clientsClaim: true,
        skipWaiting: true
      })
    ]
  }
};
